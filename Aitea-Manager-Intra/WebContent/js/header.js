/* メイン処理 */
$(function(){
	var url = $.parseURL(); // 現在のページのURLを取得

	/* 現在のページを判定 */
	if(url.pathname == "/Aitea-Manager-Intra/index.html"){
		$('#li_top').addClass("active");
	}else if(url.pathname == "/Aitea-Manager-Intra/ranking.html"){
		$('#li_rank').addClass("active");
	}else if(url.pathname == "/Aitea-Manager-Intra/statistics.html"){
		$('#li_stat').addClass("active");
	}else{
		// 個別ページなど？
	}
	$('#version').load("version.txt"); // AiteaManagerのversion表示

	/* F5無効処理 */
	$(document).on('keydown', function(e){
		if ((e.which || e.keyCode) == 116) {
			//alert("F5キーは無効化されています。");
			return false;
		}
	});

});