/* ヘッダ取得処理 */
$(function(){
	$('#header').load("header.html");
	$('#info_txt').load("information.txt");
});

/* メイン処理 */
$(function() {

	/* 問題ID検索のボタンクリックイベント */
	$('#btn').click(function() {

		/* モーダルウィンドウ表示用jQuery */
		var get_iframe_script = '<script>'
							  + '  $(function() {'
							  + '    $(".iframe").colorbox({'
							  + '      iframe:true,'
							  + '      width:"80%",'
							  + '      height:"80%",'
							  + '      opacity: 0.7'
							  + '    });'
							  + '  });'
							  + '</script>';
		$('#get_iframe_script_field').html(get_iframe_script);

		/* 各セレクタの値を取得 */
		var exam_category = $('#category').val();
		var question_id = $('#question_id').val();

		/* 接続先URLを取得してhref属性にセット */
		var url = "/Aitea-Manager-Intra/question.html?category=" + exam_category + "&id=" + question_id ;
		$('#btn').attr('href', url);

	});

	/* 回答数データ格納用変数 */
	var cnt_today = 0;
	var date_today = "今日\n";
	var cnt_1day_ago = 0;
	var date_1day_ago = "１日前\n";
	var cnt_2days_ago = 0;
	var date_2days_ago = "２日前\n";
	var cnt_3days_ago = 0;
	var date_3days_ago = "３日前\n";
	var cnt_4days_ago = 0;
	var date_4days_ago = "４日前\n";
	var cnt_5days_ago = 0;
	var date_5days_ago = "５日前\n";
	var cnt_6days_ago = 0;
	var date_6days_ago = "６日前\n";

	/* 回答数データ取得用URL生成 */
	var str = "/Aitea-Manager-Intra/weekly" ;

	/* ajaxによる非同期通信にてサーバからJSON形式のデータを取得 */
	function sync(str){
		$.ajax({
			type: 'GET',     // 通信タイプを指定(今回は取得なのでGET)
			url: str,        // データ取得先URLを指定
			dataType: 'json' // JSON形式を指定
		}).done(function(json){ // ajax通信成功時

			/* データ格納 */
			cnt_today = json[0].cnt;
			date_today += json[0].user_answer_date;
			cnt_1day_ago = json[1].cnt;
			date_1day_ago += json[1].user_answer_date;
			cnt_2days_ago = json[2].cnt;
			date_2days_ago += json[2].user_answer_date;
			cnt_3days_ago = json[3].cnt;
			date_3days_ago += json[3].user_answer_date;
			cnt_4days_ago = json[4].cnt;
			date_4days_ago += json[4].user_answer_date;
			cnt_5days_ago = json[5].cnt;
			date_5days_ago += json[5].user_answer_date;
			cnt_6days_ago = json[6].cnt;
			date_6days_ago += json[6].user_answer_date;
			draw();

		}).fail(function(json){ // ajax通信失敗時
			var error_txt = "ERROR！問題が発生しました。データを取得できません。<br>";
			error_txt += "接続先：" + str + "<br>";
			alert(error_txt);
			draw();
		});
	}
	sync(str);

	/* 一週間の回答数グラフ描画処理 */
	function draw(){
		jQuery.jqplot(
				'weekly_graph',
				[
				 [ [ date_6days_ago, cnt_6days_ago ], [ date_5days_ago, cnt_5days_ago ], [ date_4days_ago, cnt_4days_ago ], [ date_3days_ago, cnt_3days_ago ], [ date_2days_ago, cnt_2days_ago ], [ date_1day_ago, cnt_1day_ago ], [ date_today, cnt_today ] ]
				 ],
				 {
					seriesDefaults: {
						renderer: jQuery.jqplot.BarRenderer,
						pointLabels: {
							show: true,
							location: 'n',
							ypadding: -3,
							escapeHTML: false,
							formatString: '<p style="color: blue; font-size: 14px;">%d</p>'
						}
					},
					axes: {
						xaxis: {
							renderer: jQuery.jqplot.CategoryAxisRenderer,
						}
					}
				 }
		);
	}

});