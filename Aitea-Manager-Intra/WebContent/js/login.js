/* メイン処理 */
$(function(){
	$('#version').load("version.txt"); // AiteaManagerのversion表示

	/* F5無効処理 */
	$(document).on('keydown', function(e){
		if ((e.which || e.keyCode) == 116) {
			//alert("F5キーは無効化されています。");
			return false;
		}
	});
});