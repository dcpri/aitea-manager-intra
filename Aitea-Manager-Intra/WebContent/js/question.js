/* メイン処理 */
$(function() {

	/* 接続先URL取得 */
	var url = $.parseURL(); // 現在のページのURLから値を取得
	var str = "/Aitea-Manager-Intra/individually?category=" + url.query.category + "&id=" + url.query.id ;

	function sync(str){
		/* ajaxによる非同期通信にてサーバからJSON形式のデータを取得 */
		$.ajax({
			type: 'GET',     // 通信タイプを指定(今回は取得なのでGET)
			url: str,        // データ取得先URLを指定
			dataType: 'json' // JSON形式を指定
		}).done(function(json){ // ajax通信成功時

			/* 初期化 */
			var txt = "" ;
			var category = "";

			/* 区分判定 */
			if(url.query.category=="AP"){
				category = "応用情報技術者試験" ;
			}else if(url.query.category=="FE"){
				category = "基本情報技術者試験" ;
			}else if(url.query.category=="IP"){
				category = "ITパスポート試験" ;
			}else{
				category = "" ;
			}

			/* コンテンツ表示 */
			$('#question_name_value').html(json[0].question_name);
			$('#question_title').html("<h3>" + category + "　" + json[0].year + "　" + json[0].turn + "　問" + json[0].question_no + "（問題ID: " + json[0].question_id + "）の個別情報ページ</h3>");
			$('#genre_value').html(json[0].field_name + "＞" + json[0].large_class_name + "＞" + json[0].middle_class_name + "＞" + json[0].small_class_name);
			$('#answer_count_value').html(json[0].cnt + " 回");
			$('#correct_rate_value').html((Math.round(json[0].question_correct_rate*100)/100) + " %");
			$('#selection_rate_a').html((Math.round(json[0].selection_rate_a*100)/100) + " %");
			$('#selection_rate_i').html((Math.round(json[0].selection_rate_i*100)/100) + " %");
			$('#selection_rate_u').html((Math.round(json[0].selection_rate_u*100)/100) + " %");
			$('#selection_rate_e').html((Math.round(json[0].selection_rate_e*100)/100) + " %");
			$('#selection_rate_q').html((Math.round(json[0].selection_rate_q*100)/100) + " %");

			/* グラフ表示 */
			jQuery.jqplot(
				'selection_graph_value',
				[
				 	[ [ json[0].selection_rate_q, '？' ], [ json[0].selection_rate_e, 'エ' ], [ json[0].selection_rate_u, 'ウ' ], [ json[0].selection_rate_i, 'イ' ], [ json[0].selection_rate_a, 'ア' ] ]
				 ],
				 {
					seriesDefaults: {
						renderer: jQuery . jqplot . BarRenderer,
						rendererOptions: {
							barDirection: 'horizontal'
						}
					},
					axes: {
						yaxis: {
							renderer: jQuery . jqplot . CategoryAxisRenderer,
						}
					}
				 }
			);

		}).fail(function(json){ // ajax通信失敗時
			var error_txt = "ERROR！問題が発生しました。データを取得できません。<br>";
			error_txt += "接続先：" + str + "<br>";
			$('#ajax_result').html(error_txt);
		});
	}
	sync(str);
});