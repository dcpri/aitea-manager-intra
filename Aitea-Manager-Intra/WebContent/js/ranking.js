/* ヘッダ取得処理 */
$(function(){
	$('#header').load("header.html");
});

/* メイン処理用グローバル変数 */
var load = 0;
var len = 0;
var gjson = null;

/* モーダルウィンドウ表示用jQuery */
var get_iframe_script = '<script>'
					  + '  $(function() {'
					  + '    $(".iframe").colorbox({'
					  + '      iframe:true,'
					  + '      width:"80%",'
					  + '      height:"80%",'
					  + '      opacity: 0.7'
					  + '    });'
					  + '  });'
					  + '</script>';

/* メイン処理 */
$(function() {

	/* 表示ボタンクリックイベント */
	$('#btn').click(function() {

		load = 0; // ロード値初期化

		/* 各セレクタから値を取得 */
		var sort_val = $('#sort').val();
		var sort_txt = $('#sort option:selected').text();
		var order_val = $('#order').val();
		var order_txt = $('#order option:selected').text();
		var category_val = $('#category').val();
		var category_txt = $('#category option:selected').text();
		var year_val = $('#year').val();
		var year_txt = $('#year option:selected').text();
		var turn_val = $('#turn').val();
		var turn_txt = $('#turn option:selected').text();

		/* タイトル文字列とデータ取得先URLの生成 */
		var table_title_txt = "<h3>" + sort_txt + "（" + order_txt + "）ランキング</h3>";
		var str = "/Aitea-Manager-Intra/ranking?";
		    str += "sort=" + sort_val + "&order=" + order_val + "&category=" + category_val + "&year=" + year_val + "&turn=" + turn_val ;

		/* 検索条件表示 */
		if(category_val=="EMPTY" && year_val == "EMPTY" && turn_val == "EMPTY"){
			var condition_txt = "指定された条件：　無し";
		}else{
			var condition_txt = "指定された条件：<br>　";
			if(category_val != "EMPTY") condition_txt += category_txt + ", ";
			if(year_val != "EMPTY") condition_txt += year_txt + ", ";
			if(turn_val != "EMPTY") condition_txt += turn_txt + ", ";
		}
		$('#serach_condition').html(condition_txt);

		/* ajaxによる非同期通信にてサーバからJSON形式のデータを取得 */
		$.ajax({
			type: 'GET',      // 通信タイプを指定(今回は取得なのでGET)
			url: str,         // データ取得先URLを指定
			dataType: 'json', // JSON形式を指定
		}).done(function(json){ // ajax通信成功時

			/* 表示エリア初期化 */
			$('tbody').html("");
			$('#reload').show();
			$('#table_title').html(table_title_txt);

			/* 取得したJSON形式データをグローバル変数に格納 */
			gjson = json;

			/* ランキング表示 */
			$('#question_list').show();
			draw();
			$('#ajax_result').html("データ取得が完了しました。");
		}).fail(function(json){ // ajax通信失敗時
			$('#ajax_result').html(str + " からデータを取得できません。");
		});
	});

	/* 描画処理関数 */
	function draw() {
		var tag = '';
		var genre_str ="";
		var query = "";
		var order = "";
		len = gjson.length;

		for(var i=load; i < load+20; i++){
			genre_str = gjson[i].field_name + "＞" + gjson[i].large_class_name + "＞" + gjson[i].middle_class_name + "＞" + gjson[i].small_class_name ;
			query = "category=" + gjson[i].exam_category + "&id=" + gjson[i].question_id ;
			tag += '<tr>'
			    +  '<td>' + (i+1) + '</td>'
				+  '<td><a class="iframe" href="http://192.168.1.38:8080/Aitea-Manager-Intra/question.html?' + query + '">' + gjson[i].question_id + '</a></td>'
				+  '<td>' + gjson[i].exam_category + '</td>'
				+  '<td>' + gjson[i].year + '</td>'
				+  '<td>' + gjson[i].turn + '</td>'
				+  '<td>' + gjson[i].question_no + '</td>'
			    +  '<td>' + gjson[i].question_name + '</td>'
			    +  '<td>' + genre_str + '</td>'
			    +  '<td align="center">' + gjson[i].cnt + ' 回</td>'
			    +  '<td align="center">' + (Math.round(gjson[i].question_correct_rate*100)/100) + ' %</td>'
			    +  '</tr>';
		}
		$('tbody').append(tag);
		$('#get_iframe_script_field').html(get_iframe_script);
		load+=20;
	}

	/* リロードボタンクリックイベント */
	$('#reload').click(function() {
		if(load<=len){
			draw();
		}
		if(load>=len){
			$('#reload').hide();
		}
	});

//	/* ソート関数(無くてもいいことに気付いたからそのうち消す) */
//	var sort_by = function(field, reverse, primer){
//		reverse = (reverse) ? -1 : 1;
//		return function(a,b){
//			a = a[field];
//			b = b[field];
//			if (typeof(primer) != 'undefined'){
//				a = primer(a);
//				b = primer(b);
//			}
//			if (a<b) return reverse * -1;
//			if (a>b) return reverse * 1;
//			return 0;
//		}
//	}

});

