/* ヘッダ取得処理 */
$(function(){
	$('#header').load("header.html");
});

/* メイン処理 */
$(function() {

	/* 表示ボタンクリックイベント */
	$('#btn').click(function() {

		/* 各セレクタから値を取得 */
		var category_val = $('#category').val();
		var category_txt = $('#category option:selected').text();
		var year_val = $('#year').val();
		var year_txt = $('#year option:selected').text();
		var turn_val = $('#turn').val();
		var turn_txt = $('#turn option:selected').text();

		/* タイトル文字列とデータ取得先URLの生成 */
		var list_name_txt = "<h3>" + category_txt + "　" + year_txt + "度　" + turn_txt + "　の問題一覧</h3>";
		var str = "/Aitea-Manager-Intra/yearly?";
		str += "category=" + category_val + "&year=" + year_val + "&turn=" + turn_val ;

		/* ajaxによる非同期通信にてサーバからJSON形式のデータを取得 */
		$.ajax({
			type: 'GET',      // 通信タイプを指定(今回は取得なのでGET)
			url: str,         // データ取得先URLを指定
			dataType: 'json', // JSON形式を指定
			success: function(json){ // ajax通信成功時

				/* ページ内各要素表示処理 */
				$('#table_title').html(list_name_txt);
				$('#question_list').show();
				var len = json.length;

				/* モーダルウィンドウ表示用jQuery */
				var get_iframe_script = '<script>'
									  + '  $(function() {'
									  + '    $(".iframe").colorbox({'
									  + '      iframe:true,'
									  + '      width:"80%",'
									  + '      height:"80%",'
									  + '      opacity: 0.7'
									  + '    });'
									  + '  });'
									  + '</script>';

				function draw() {
					var genre_str ="";
					var tag="";
					for(var i=0; i < len; i++){
						genre_str = json[i].field_name + "＞" + json[i].large_class_name + "＞" + json[i].middle_class_name + "＞" + json[i].small_class_name ;
						var query = "category=" + category_val + "&id=" + json[i].question_id ;
						tag += '<tr>'
							+  '<td>' + json[i].question_no + '</td>'
							+  '<td><a class="iframe" href="http://192.168.1.38:8080/Aitea-Manager-Intra/question.html?' + query + '">' + json[i].question_id + '</a></td>'
							+  '<td>' + json[i].question_name + '</td>'
							+  '<td>' + genre_str + '</td>'
							+  '<td align="center">' + json[i].cnt + ' 回</td>'
							+  '<td align="center">' + (Math.round(json[i].question_correct_rate*100)/100) + ' %</td>'
							+  '</tr>';
					}
					$('tbody').html(tag);
					$('#get_iframe_script_field').html(get_iframe_script);
				}

				draw();
				$('#ajax_result').html("データ取得が完了しました。");
			},
			error: function() { // ajax通信失敗時
				$('#ajax_result').html(str + " からデータを取得できません。");
			}
		});
	});
});