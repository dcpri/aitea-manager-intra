// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CalcCorrectRateBeans.java

package beans;


public class CalcCorrectRateBeans
{

    public CalcCorrectRateBeans()
    {
        question_id = null;
        exam_category = null;
    }

    public String getQuestion_id()
    {
        return question_id;
    }

    public void setQuestion_id(String question_id)
    {
        this.question_id = question_id;
    }

    public String getExam_category()
    {
        return exam_category;
    }

    public void setExam_category(String exam_category)
    {
        this.exam_category = exam_category;
    }

    private String question_id;
    private String exam_category;
}
