// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   IndividuallyPageBeans.java

package beans;


public class IndividuallyPageBeans
{

    public IndividuallyPageBeans()
    {
        question_id = null;
        year = null;
        turn = null;
        question_no = null;
        field_name = null;
        large_class_name = null;
        middle_class_name = null;
        small_class_name = null;
        question_correct_rate = null;
        question_name = null;
        cnt = null;
        selection_rate_a = null;
        selection_rate_i = null;
        selection_rate_u = null;
        selection_rate_e = null;
        selection_rate_q = null;
    }

    public String getQuestion_id()
    {
        return question_id;
    }

    public void setQuestion_id(String question_id)
    {
        this.question_id = question_id;
    }

    public String getYear()
    {
        return year;
    }

    public void setYear(String year)
    {
        this.year = year;
    }

    public String getTurn()
    {
        return turn;
    }

    public void setTurn(String turn)
    {
        this.turn = turn;
    }

    public Integer getQuestion_no()
    {
        return question_no;
    }

    public void setQuestion_no(Integer question_no)
    {
        this.question_no = question_no;
    }

    public String getField_name()
    {
        return field_name;
    }

    public void setField_name(String field_name)
    {
        this.field_name = field_name;
    }

    public String getLarge_class_name()
    {
        return large_class_name;
    }

    public void setLarge_class_name(String large_class_name)
    {
        this.large_class_name = large_class_name;
    }

    public String getMiddle_class_name()
    {
        return middle_class_name;
    }

    public void setMiddle_class_name(String middle_class_name)
    {
        this.middle_class_name = middle_class_name;
    }

    public String getSmall_class_name()
    {
        return small_class_name;
    }

    public void setSmall_class_name(String small_class_name)
    {
        this.small_class_name = small_class_name;
    }

    public Double getQuestion_correct_rate()
    {
        return question_correct_rate;
    }

    public void setQuestion_correct_rate(Double question_correct_rate)
    {
        this.question_correct_rate = question_correct_rate;
    }

    public Double getSelection_rate_a()
    {
        return selection_rate_a;
    }

    public void setSelection_rate_a(Double selection_rate_a)
    {
        this.selection_rate_a = selection_rate_a;
    }

    public Double getSelection_rate_i()
    {
        return selection_rate_i;
    }

    public void setSelection_rate_i(Double selection_rate_i)
    {
        this.selection_rate_i = selection_rate_i;
    }

    public Double getSelection_rate_u()
    {
        return selection_rate_u;
    }

    public void setSelection_rate_u(Double selection_rate_u)
    {
        this.selection_rate_u = selection_rate_u;
    }

    public Double getSelection_rate_e()
    {
        return selection_rate_e;
    }

    public void setSelection_rate_e(Double selection_rate_e)
    {
        this.selection_rate_e = selection_rate_e;
    }

    public Double getSelection_rate_q()
    {
        return selection_rate_q;
    }

    public void setSelection_rate_q(Double selection_rate_q)
    {
        this.selection_rate_q = selection_rate_q;
    }

    public String getQuestion_name()
    {
        return question_name;
    }

    public void setQuestion_name(String question_name)
    {
        this.question_name = question_name;
    }

    public Integer getCnt()
    {
        return cnt;
    }

    public void setCnt(Integer cnt)
    {
        this.cnt = cnt;
    }

    public String question_id;
    public String year;
    public String turn;
    public Integer question_no;
    public String field_name;
    public String large_class_name;
    public String middle_class_name;
    public String small_class_name;
    public Double question_correct_rate;
    public String question_name;
    public Integer cnt;
    public Double selection_rate_a;
    public Double selection_rate_i;
    public Double selection_rate_u;
    public Double selection_rate_e;
    public Double selection_rate_q;
}
