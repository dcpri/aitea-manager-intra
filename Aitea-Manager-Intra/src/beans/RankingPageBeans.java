// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   RankingPageBeans.java

package beans;


public class RankingPageBeans
{

    public RankingPageBeans()
    {
        exam_category = null;
        question_id = null;
        year = null;
        turn = null;
        question_no = null;
        field_name = null;
        large_class_name = null;
        middle_class_name = null;
        small_class_name = null;
        question_name = null;
        cnt = null;
        question_correct_rate = null;
    }

    public String getExam_category()
    {
        return exam_category;
    }

    public void setExam_category(String exam_category)
    {
        this.exam_category = exam_category;
    }

    public String getQuestion_id()
    {
        return question_id;
    }

    public void setQuestion_id(String question_id)
    {
        this.question_id = question_id;
    }

    public String getYear()
    {
        return year;
    }

    public void setYear(String year)
    {
        this.year = year;
    }

    public String getTurn()
    {
        return turn;
    }

    public void setTurn(String turn)
    {
        this.turn = turn;
    }

    public Integer getQuestion_no()
    {
        return question_no;
    }

    public void setQuestion_no(Integer question_no)
    {
        this.question_no = question_no;
    }

    public String getField_name()
    {
        return field_name;
    }

    public void setField_name(String field_name)
    {
        this.field_name = field_name;
    }

    public String getLarge_class_name()
    {
        return large_class_name;
    }

    public void setLarge_class_name(String large_class_name)
    {
        this.large_class_name = large_class_name;
    }

    public String getMiddle_class_name()
    {
        return middle_class_name;
    }

    public void setMiddle_class_name(String middle_class_name)
    {
        this.middle_class_name = middle_class_name;
    }

    public String getSmall_class_name()
    {
        return small_class_name;
    }

    public void setSmall_class_name(String small_class_name)
    {
        this.small_class_name = small_class_name;
    }

    public String getQuestion_name()
    {
        return question_name;
    }

    public void setQuestion_name(String question_name)
    {
        this.question_name = question_name;
    }

    public Integer getCnt()
    {
        return cnt;
    }

    public void setCnt(Integer cnt)
    {
        this.cnt = cnt;
    }

    public Double getQuestion_correct_rate()
    {
        return question_correct_rate;
    }

    public void setQuestion_correct_rate(Double question_correct_rate)
    {
        this.question_correct_rate = question_correct_rate;
    }

    public String exam_category;
    public String question_id;
    public String year;
    public String turn;
    public Integer question_no;
    public String field_name;
    public String large_class_name;
    public String middle_class_name;
    public String small_class_name;
    public String question_name;
    public Integer cnt;
    public Double question_correct_rate;
}
