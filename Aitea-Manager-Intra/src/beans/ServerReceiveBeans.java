// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   ServerReceiveBeans.java

package beans;


public class ServerReceiveBeans
{

    public ServerReceiveBeans()
    {
        receive_id = null;
        question_id = null;
        user_answer = null;
        exam_category = null;
        correct_mistake = null;
        user_answer_date = null;
    }

    public String getReceive_id()
    {
        return receive_id;
    }

    public void setReceive_id(String receive_id)
    {
        this.receive_id = receive_id;
    }

    public String getQuestion_id()
    {
        return question_id;
    }

    public void setQuestion_id(String question_id)
    {
        this.question_id = question_id;
    }

    public String getUser_answer()
    {
        return user_answer;
    }

    public void setUser_answer(String user_answer)
    {
        this.user_answer = user_answer;
    }

    public String getExam_category()
    {
        return exam_category;
    }

    public void setExam_category(String exam_category)
    {
        this.exam_category = exam_category;
    }

    public Integer getCorrect_mistake()
    {
        return correct_mistake;
    }

    public void setCorrect_mistake(Integer correct_mistake)
    {
        this.correct_mistake = correct_mistake;
    }

    public String getUser_answer_date()
    {
        return user_answer_date;
    }

    public void setUser_answer_date(String user_answer_date)
    {
        this.user_answer_date = user_answer_date;
    }

    private String receive_id;
    private String question_id;
    private String user_answer;
    private String exam_category;
    private Integer correct_mistake;
    private String user_answer_date;
}
