// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   WeeklyGraphBeans.java

package beans;


public class WeeklyGraphBeans
{

    public WeeklyGraphBeans()
    {
        cnt = null;
        user_answer_date = null;
    }

    public Integer getCnt()
    {
        return cnt;
    }

    public void setCnt(Integer cnt)
    {
        this.cnt = cnt;
    }

    public String getUser_answer_date()
    {
        return user_answer_date;
    }

    public void setUser_answer_date(String user_answer_date)
    {
        this.user_answer_date = user_answer_date;
    }

    private Integer cnt;
    private String user_answer_date;
}
