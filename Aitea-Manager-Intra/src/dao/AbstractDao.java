package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

public abstract class AbstractDao
{

    /**
     * データソース
     */
    @SuppressWarnings("unused")
	private DataSource source;

    /**
     * @param source データベースのソース
     */
    public AbstractDao(DataSource source)
    {
        this.source = source;
    }


    /**
     * @param conn データベースコネクション
     * コミット操作を行う
     */
    protected void commit(Connection conn)
    {
        if(conn != null)
            try
            {
                conn.commit();
            }
            catch(SQLException sqlexception) { }
    }


    /**
     * @param conn データベースコネクション
     * ロールバック操作を行う
     */
    protected void rollback(Connection conn)
    {
        if(conn != null)
            try
            {
                conn.close();
            }
            catch(SQLException sqlexception) { }
    }


    /**
     * @param conn データベースコネクション
     * データベースコネクションのクローズ操作を行う
     */
    protected void close(Connection conn)
    {
        if(conn != null)
            try
            {
                conn.close();
            }
            catch(SQLException sqlexception) { }
    }


    /**
     * @param statement プリペアドステートメント
     * プリペアドステートメントのクローズ操作を行う
     */
    protected void close(PreparedStatement statement)
    {
        if(statement != null)
            try
            {
                statement.close();
            }
            catch(SQLException sqlexception) { }
    }


    /**
     * @param rs リザルトセット
     * リザルトセットのクローズ操作を行う
     */
    protected void close(ResultSet rs)
    {
        if(rs != null)
            try
            {
                rs.close();
            }
            catch(SQLException sqlexception) { }
    }
}
