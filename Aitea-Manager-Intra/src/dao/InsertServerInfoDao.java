package dao;

import beans.ServerReceiveBeans;

public interface InsertServerInfoDao
{

    /**
     * @return 受信ID
     * サーバ受信テーブルの総件数を計算し、空いている受信IDの中から使用できる
     * 最も若い受信IDを返却する
     */
    public abstract int examineReceiveId();


    /**
     * @param serverreceivebeans サーバ受信テーブルのBean
     * @return 成功可否
     * スマホから受け取ったサーバ送信情報を、サーバ受信テーブルに格納する
     */
    public abstract boolean insertServerReceiveInfo(ServerReceiveBeans serverreceivebeans);
}
