package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import beans.ServerReceiveBeans;


public class JdbcInsertServerInfoDao extends AbstractDao
    implements InsertServerInfoDao
{

    public JdbcInsertServerInfoDao(DataSource source)
    {
        super(source);
    }

    public int examineReceiveId()
    {
        ResultSet rs = null;
    	PreparedStatement pstmt = null;
        Connection conn = DaoFactory.getConnectionPostgre();
        int cnt = 0;

        try
        {
            pstmt = SQLSet.query_examinReceiveId(conn);
            // レコード総数をカウントし取得
            for(rs = pstmt.executeQuery(); rs.next();)
                cnt = rs.getInt("cnt");
            // レコード総数+1が登録すべき受信ID
            cnt++;
        }
        catch(SQLException e)
        {
            rollback(conn);
            e.printStackTrace();
        }
        finally
        {
        	close(rs);
        	close(pstmt);
        	close(conn);
        }
        return cnt;
    }

    public boolean insertServerReceiveInfo(ServerReceiveBeans bean)
    {
    	PreparedStatement pstmt = null;
        Connection conn = DaoFactory.getConnectionPostgre();
        boolean ret = false;


        try
        {
            conn = DaoFactory.getConnectionPostgre();
            pstmt = SQLSet.insert_ServerReceiveInfo(conn, bean);
            pstmt.executeUpdate();
        }
        catch(SQLException e)
        {
            rollback(conn);
        }
        finally
        {
        close(pstmt);
        close(conn);
        }
        return ret;
    }
}
