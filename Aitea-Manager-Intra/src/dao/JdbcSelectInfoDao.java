package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.sql.DataSource;

import beans.IndividuallyPageBeans;
import beans.RankingPageBeans;
import beans.WeeklyGraphBeans;
import beans.YearlyPageBeans;


public class JdbcSelectInfoDao extends AbstractDao
    implements SelectInfoDao
{

    public JdbcSelectInfoDao(DataSource source)
    {
        super(source);
    }

    public ArrayList<YearlyPageBeans> selectYearly(String category, String year, String turn)
    {
        ResultSet rs = null;
    	PreparedStatement pstmt = null;
        Connection conn = DaoFactory.getConnectionPostgre();
        ArrayList<YearlyPageBeans> list = new ArrayList<>();

        try
        {
        	pstmt = SQLSet.query_Yearly(conn, category, year, turn);

            YearlyPageBeans bean;
            for(rs = pstmt.executeQuery(); rs.next(); list.add(bean))
            {
                bean = new YearlyPageBeans();
                bean.setQuestion_id(rs.getString("question_id"));
                bean.setQuestion_no(Integer.valueOf(rs.getInt("question_no")));
                bean.setQuestion_name(rs.getString("question_name"));
                bean.setField_name(rs.getString("field_name"));
                bean.setLarge_class_name(rs.getString("large_class_name"));
                bean.setMiddle_class_name(rs.getString("middle_class_name"));
                bean.setSmall_class_name(rs.getString("small_class_name"));
                bean.setCnt(Integer.valueOf(rs.getInt("cnt")));
                bean.setQuestion_correct_rate(Double.valueOf(rs.getDouble("question_correct_rate")));
            }

        }
        catch(SQLException e)
        {
            rollback(conn);
        }
        finally
        {
        	close(rs);
        	close(pstmt);
        	close(conn);
        }
        return list;
    }

    public ArrayList<IndividuallyPageBeans> selectIndividually(String category, String id)
    {
        ResultSet rs = null;
    	PreparedStatement pstmt = null;
        Connection conn = DaoFactory.getConnectionPostgre();
        ArrayList<IndividuallyPageBeans> list = new ArrayList<>();

        try
        {
            pstmt = SQLSet.query_Individually(conn, category, id);

            IndividuallyPageBeans bean;
            for(rs = pstmt.executeQuery(); rs.next(); list.add(bean))
            {
                bean = new IndividuallyPageBeans();
                bean.setQuestion_id(rs.getString("question_id"));
                bean.setYear(rs.getString("year"));
                bean.setTurn(rs.getString("turn"));
                bean.setQuestion_no(Integer.valueOf(rs.getInt("question_no")));
                bean.setField_name(rs.getString("field_name"));
                bean.setLarge_class_name(rs.getString("large_class_name"));
                bean.setMiddle_class_name(rs.getString("middle_class_name"));
                bean.setSmall_class_name(rs.getString("small_class_name"));
                bean.setQuestion_correct_rate(Double.valueOf(rs.getDouble("question_correct_rate")));
                bean.setQuestion_name(rs.getString("question_name"));
                bean.setCnt(Integer.valueOf(rs.getInt("cnt")));
                bean.setSelection_rate_a(Double.valueOf(rs.getDouble("selection_rate_a")));
                bean.setSelection_rate_i(Double.valueOf(rs.getDouble("selection_rate_i")));
                bean.setSelection_rate_u(Double.valueOf(rs.getDouble("selection_rate_u")));
                bean.setSelection_rate_e(Double.valueOf(rs.getDouble("selection_rate_e")));
                bean.setSelection_rate_q(Double.valueOf(rs.getDouble("selection_rate_q")));
            }

        }
        catch(SQLException e)
        {
            rollback(conn);
        }
        finally{
        	close(rs);
        	close(pstmt);
        	close(conn);
        }
        return list;
    }

    public ArrayList<RankingPageBeans> selectRanking(String sort, String category, String year, String turn)
    {
        ResultSet rs = null;
    	PreparedStatement pstmt = null;
        Connection conn = DaoFactory.getConnectionPostgre();
        ArrayList<RankingPageBeans> list = new ArrayList<>();

        try
        {
        	pstmt = SQLSet.query_Ranking(conn, sort, category, year, turn);
            RankingPageBeans bean;
            for(rs = pstmt.executeQuery(); rs.next(); list.add(bean))
            {
                bean = new RankingPageBeans();
                bean.setExam_category(rs.getString("exam_category"));
                bean.setQuestion_id(rs.getString("question_id"));
                bean.setYear(rs.getString("year"));
                bean.setTurn(rs.getString("turn"));
                bean.setQuestion_no(Integer.valueOf(rs.getInt("question_no")));
                bean.setQuestion_name(rs.getString("question_name"));
                bean.setField_name(rs.getString("field_name"));
                bean.setLarge_class_name(rs.getString("large_class_name"));
                bean.setMiddle_class_name(rs.getString("middle_class_name"));
                bean.setSmall_class_name(rs.getString("small_class_name"));
                bean.setCnt(Integer.valueOf(rs.getInt("cnt")));
                bean.setQuestion_correct_rate(Double.valueOf(rs.getDouble("question_correct_rate")));
            }
        }
        catch(SQLException e)
        {
            rollback(conn);
        }
        finally
        {
        	close(rs);
        	close(pstmt);
        	close(conn);
        }
        return list;
    }

    public ArrayList<WeeklyGraphBeans> selectWeekly()
    {
        ResultSet rs = null;
    	PreparedStatement pstmt = null;
        Connection conn = DaoFactory.getConnectionPostgre();
        ArrayList<WeeklyGraphBeans> list = new ArrayList<>();

        try
        {
            pstmt = SQLSet.query_Weekly(conn);
            rs = pstmt.executeQuery();
            String dates[] = getDates();
            for(int i = 0; i < 7; i++)
            {
                WeeklyGraphBeans bean = new WeeklyGraphBeans();
                bean.setCnt(Integer.valueOf(0));
                bean.setUser_answer_date(dates[i]);
                list.add(bean);
            }

            while(rs.next())
            {
                int dayCount = 0;
                for(dayCount = 0; dayCount < 7; dayCount++)
                    if(rs.getString("user_answer_date").equals(dates[dayCount]))
                        break;

                if(dayCount != 7)
                    ((WeeklyGraphBeans)list.get(dayCount)).setCnt(Integer.valueOf(rs.getInt("cnt")));
            }
        }
        catch(SQLException e)
        {
            rollback(conn);
        }
        finally
        {
        	close(rs);
        	close(pstmt);
        	close(conn);
        }
        return list;
    }


    /**
     * @return 日付のリスト
     * 実行日時を基準に、前一週間の日付をYYYY-MM-DD形式で取得し、
     * リストで返却する
     */
    private String[] getDates()
    {
        String dates[] = new String[7];
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(5, 1);
        for(int i = 0; i < 7; i++)
        {
            cal.add(5, -1);
            dates[i] = format.format(cal.getTime());
        }

        return dates;
    }

}
