package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.sql.DataSource;

import beans.CalcCorrectRateBeans;

/**
 * @author joho
 * 回答率、選択肢選択率の更新を行う
 */
public class JdbcUpdateRateDao extends AbstractDao implements UpdateRateDao {

	public JdbcUpdateRateDao(DataSource source) {
		super(source);
	}

	/* (非 Javadoc)
	 * @see dao.UpdateRateDao#selectUpdateQuestion()
	 */
	public ArrayList<CalcCorrectRateBeans> selectUpdateQuestion() {
        ResultSet rs = null;
    	PreparedStatement pstmt = null;
        Connection conn = DaoFactory.getConnectionPostgre();
		ArrayList<CalcCorrectRateBeans> list = new ArrayList<>();

		try {
			pstmt = SQLSet.query_UpdateQuestion(conn);
			CalcCorrectRateBeans bean;
			for (rs = pstmt.executeQuery(); rs.next(); list.add(bean)) {
				bean = new CalcCorrectRateBeans();
				bean.setExam_category(rs.getString("exam_category"));
				bean.setQuestion_id(rs.getString("question_id"));
			}
		} catch (SQLException e) {
			rollback(conn);
		} finally {
			close(rs);
			close(pstmt);
			close(conn);
		}
		return list;
	}

	public HashMap<String, Integer> selectSelectionRateAll(String exam_category, String question_id) {
        ResultSet rs = null;
    	PreparedStatement pstmt = null;
        Connection conn = DaoFactory.getConnectionPostgre();
		HashMap<String, Integer> map = new HashMap<>();

		try {
			pstmt = SQLSet.query_SelectionRateAll(conn, exam_category, question_id);

			for (rs = pstmt.executeQuery(); rs.next(); map.put(rs.getString("user_answer"),
					Integer.valueOf(rs.getInt("cnt"))));
		} catch (SQLException e) {
			rollback(conn);
			e.printStackTrace();
		} finally {
			close(rs);
			close(pstmt);
			close(conn);
		}
		return map;
	}

	public boolean updateRates(String exam_category,
			String question_id, HashMap<String, Integer> map) {
    	PreparedStatement pstmt = null;
        Connection conn = DaoFactory.getConnectionPostgre();
		int result = 0;

		try {
			pstmt = SQLSet.update_Rates(conn, exam_category, question_id, map);

			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			rollback(conn);
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(conn);
		}
		return result == 1;
	}
}
