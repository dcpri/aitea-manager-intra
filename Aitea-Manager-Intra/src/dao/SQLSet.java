/**
 *
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import beans.ServerReceiveBeans;

/**
 * @author joho
 *
 */
public final class SQLSet {

	public static final PreparedStatement insert_ServerReceiveInfo(Connection conn,
			ServerReceiveBeans bean) throws SQLException{
		PreparedStatement pstmt = null;
		String sql = "INSERT INTO tbl_server_receive_info VALUES (?,?,?,?,?,?)";

        try {
			pstmt = conn.prepareStatement(sql);
            // 主キーはnot null
            pstmt.setString(1, bean.getReceive_id());

            // 各項目についてnullチェック
            if(bean.getQuestion_id() == null){
            	pstmt.setNull(2, java.sql.Types.CHAR);
            } else {
            	pstmt.setString(2, bean.getQuestion_id());
            }

            if(bean.getUser_answer() == null){
                pstmt.setNull(3, java.sql.Types.CHAR);
            } else {
                pstmt.setString(3, bean.getUser_answer());
            }

            if(bean.getExam_category() == null){
                pstmt.setNull(4, java.sql.Types.CHAR);
            } else {
                pstmt.setString(4, bean.getExam_category());
            }

            if(bean.getCorrect_mistake() == null){
                pstmt.setNull(5, java.sql.Types.INTEGER);
            } else {
                pstmt.setInt(5, bean.getCorrect_mistake().intValue());
            }

            pstmt.setString(6, getCurrentTimeStamp());
		} catch (SQLException e) {
			throw new SQLException();
		}
		return pstmt;
	}

	public static final PreparedStatement query_examinReceiveId(Connection conn) throws SQLException{
		PreparedStatement pstmt = null;
        String sql = "select count(receive_id) as cnt from public.tbl_server_receive_info";

        try {
			pstmt = conn.prepareStatement(sql);
		} catch (SQLException e) {
			throw new SQLException();
		}
		return pstmt;
	}

	public static final PreparedStatement update_Rates(Connection conn,
			String exam_category, String question_id, HashMap<String, Integer> map) throws SQLException{
		PreparedStatement pstmt = null;
		String sql =
				"update tbl_question_info"
				+ " set question_correct_rate = cast(correct.cnt as dec) * 100 / denominator.cnt";
		String selections[] = { "ア", "イ", "ウ", "エ", "？" };

		HashMap<String, String> sqlmap = new HashMap<>();

		for (int i = 0; i < selections.length; i++) {
			String s = selections[i];
			sqlmap.put(s, "");
			Integer c = null;
			if ((c = (Integer) map.get(s)) != null)
				sqlmap.put(s, createSelectionSQL(s, c));
			else
				sqlmap.put(s, createSelectionSQL(s, Integer.valueOf(0)));
		}

		for (int j = 0; j < selections.length; j++) {
			String s = selections[j];
			String appendSql = null;
			if ((appendSql = (String) sqlmap.get(s)).trim().length() != 0)
				sql = (new StringBuilder(String.valueOf(sql))).append(",").append(appendSql).toString();
		}

		sql = (new StringBuilder(String.valueOf(sql)))
				.append(
						" from (select count(*) as cnt"
						+ " from (select * from tbl_server_receive_info"
						+ " where exam_category = ? and question_id = ?) as receive)"
						+ " as denominator, (select count(*)"
						+ " as cnt from (select * from tbl_server_receive_info"
						+ " where exam_category = ? and question_id = ? and correct_mistake = 1) as result) as correct"
						+ " where exam_category = ? and question_id = ?")
				.toString();
        try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, exam_category);
			pstmt.setString(2, question_id);
			pstmt.setString(3, exam_category);
			pstmt.setString(4, question_id);
			pstmt.setString(5, exam_category);
			pstmt.setString(6, question_id);
		} catch (SQLException e) {
			throw new SQLException();
		}
		return pstmt;
	}

	public static final PreparedStatement query_SelectionRateAll(Connection conn,
			String exam_category, String question_id) throws SQLException{
		PreparedStatement pstmt = null;
		String sql =
				"select user_answer, count(*) as cnt"
				+ " from public.tbl_server_receive_info"
				+ " where exam_category = ? and question_id = ? group by user_answer";
        try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, exam_category);
			pstmt.setString(2, question_id);
		} catch (SQLException e) {
			throw new SQLException();
		}
		return pstmt;
	}

	public static final PreparedStatement query_UpdateQuestion(Connection conn) throws SQLException{
		PreparedStatement pstmt = null;
		String sql =
				"select distinct comp_result.question_id, comp_result.exam_category"
				+ " from( select result.question_id, result.exam_category, (result.update_time >= regist_time)"
				+ " as comp1, ((result.update_time - CAST('1 day' AS INTERVAL)) < regist_time) as comp2"
				+ " from( select question_id, exam_category, (CAST(current_date AS TIMESTAMP) +"
				+ " CAST('86399 seconds' AS INTERVAL)) as update_time,"
				+ " CAST(user_answer_date AS TIMESTAMP) as regist_time"
				+ " from public.tbl_server_receive_info ) as result )"
				+ " as comp_result where comp_result.comp1 and comp_result.comp2";
        try {
			pstmt = conn.prepareStatement(sql);
		} catch (SQLException e) {
			throw new SQLException();
		}
		return pstmt;
	}

	public static final PreparedStatement query_Yearly(Connection conn, String category,
			String year, String turn) throws SQLException{
		PreparedStatement pstmt = null;
        String sql =
        		"select question_id, question_no, question_name, field_name, large_class_name,"
        		+ " middle_class_name, small_class_name, result.cnt as cnt, question_correct_rate"
        		+ " from (select qi.question_id, question_no, question_name, field_name,"
        		+ " large_class_name, middle_class_name, small_class_name, sri.cnt, question_correct_rate"
        		+ " from tbl_question_info as qi"
        		+ " inner join tbl_genre_info as gi on qi.exam_category = gi.exam_category"
        		+ " and qi.field_code = gi.field_code"
        		+ " and qi.large_class_code = gi.large_class_code"
        		+ " and qi.middle_class_code = gi.middle_class_code"
        		+ " and qi.small_class_code = gi.small_class_code"
        		+ " left join ( select question_id, count(question_id) as cnt"
        		+ " from tbl_server_receive_info group by question_id )as sri"
        		+ " on qi.question_id = sri.question_id"
        		+ " where qi.exam_category = ? and year = ? and turn = ? ) as result"
        		+ " order by question_no;";
        try {
			pstmt = conn.prepareStatement(sql);
	        pstmt.setString(1, category);
	        pstmt.setString(2, year);
	        pstmt.setString(3, turn);
		} catch (SQLException e) {
			throw new SQLException();
		}
		return pstmt;
	}


	public static final PreparedStatement query_Individually(Connection conn,
			String category, String id) throws SQLException{
		PreparedStatement pstmt = null;
        String sql =
        		"select exam_category, question_id, turn, year, question_no,"
        		+ " question_name, field_name, large_class_name, middle_class_name,"
        		+ " small_class_name, result.cnt as cnt, question_correct_rate, selection_rate_a,"
        		+ " selection_rate_i, selection_rate_u, selection_rate_e, selection_rate_q"
        		+ " from ( select qi.exam_category, qi.question_id, turn, year, question_no,"
        		+ " question_name, field_name, large_class_name, middle_class_name,"
        		+ " small_class_name, sri.cnt, question_correct_rate,  selection_rate_a,"
        		+ " selection_rate_i, selection_rate_u, selection_rate_e, selection_rate_q"
        		+ " from tbl_question_info as qi inner join tbl_genre_info as gi"
        		+ " on qi.exam_category = gi.exam_category"
        		+ " and qi.field_code = gi.field_code"
        		+ " and qi.large_class_code = gi.large_class_code"
        		+ " and qi.middle_class_code = gi.middle_class_code"
        		+ " and qi.small_class_code = gi.small_class_code"
        		+ " left join ( select question_id, count(question_id) as cnt"
        		+ " from tbl_server_receive_info group by question_id ) as sri"
        		+ " on qi.question_id = sri.question_id ) as result"
        		+ " where result.exam_category = ? and result.question_id = ?";
        try {
			pstmt = conn.prepareStatement(sql);
	        pstmt.setString(1, category);
	        pstmt.setString(2, id);
		} catch (SQLException e) {
			throw new SQLException();
		}
		return pstmt;
	}

	public static final PreparedStatement query_Ranking(Connection conn,
			String sort, String category, String year, String turn) throws SQLException{
		PreparedStatement pstmt = null;
        String sql =
        		"select exam_category, question_id, turn, year, question_no,"
        		+ " question_name, field_name, large_class_name, middle_class_name,"
        		+ " small_class_name, result.cnt as cnt, question_correct_rate"
        		+ " from ( select qi.exam_category, qi.question_id, turn, year, question_no,"
        		+ " question_name, field_name, large_class_name, middle_class_name,"
        		+ " small_class_name, sri.cnt, question_correct_rate"
        		+ " from tbl_question_info as qi"
        		+ " inner join tbl_genre_info as gi"
        		+ " on qi.exam_category = gi.exam_category"
        		+ " and qi.field_code = gi.field_code"
        		+ " and qi.large_class_code = gi.large_class_code"
        		+ " and qi.middle_class_code = gi.middle_class_code"
        		+ " and qi.small_class_code = gi.small_class_code"
        		+ " left join ( select question_id, count(question_id) as cnt"
        		+ " from tbl_server_receive_info group by question_id ) as sri"
        		+ " on qi.question_id = sri.question_id";
        try {
            int category_place = 0;
            int year_place = 0;
            int turn_place = 0;
            int place = 1;
            String where_phrase = " where";
            if(!category.equals("EMPTY"))
            {
                where_phrase = (new StringBuilder(String.valueOf(where_phrase))).append(" qi.exam_category = ?").toString();
                category_place = place++;
            }
            if(!year.equals("EMPTY"))
            {
                if(place != 1)
                    where_phrase = (new StringBuilder(String.valueOf(where_phrase))).append(" and").toString();
                where_phrase = (new StringBuilder(String.valueOf(where_phrase))).append(" year = ?").toString();
                year_place = place++;
            }
            if(!turn.equals("EMPTY"))
            {
                if(place != 1)
                    where_phrase = (new StringBuilder(String.valueOf(where_phrase))).append(" and").toString();
                where_phrase = (new StringBuilder(String.valueOf(where_phrase))).append(" turn = ?").toString();
                turn_place = place;
            }
            if(where_phrase.equals(" where"))
                sql = (new StringBuilder(String.valueOf(sql))).append(" ) as result order by ").append(sort).append(" limit 100").toString();
            else
                sql = (new StringBuilder(String.valueOf(sql))).append(where_phrase).append(" ) as result order by ").append(sort).append(", year desc, turn, question_no limit 100").toString();
            pstmt = conn.prepareStatement(sql);
            if(!category.equals("EMPTY"))
                pstmt.setString(category_place, category);
            if(!year.equals("EMPTY"))
                pstmt.setString(year_place, year);
            if(!turn.equals("EMPTY"))
                pstmt.setString(turn_place, turn);
		} catch (SQLException e) {
			throw new SQLException();
		}
		return pstmt;
	}

	public static final PreparedStatement query_Weekly(Connection conn) throws SQLException{
		PreparedStatement pstmt = null;
        String sql =
        		"select CAST(user_answer_date as date), count(*) as cnt"
        		+ " from tbl_server_receive_info group by CAST(user_answer_date as date)"
        		+ " having CAST(user_answer_date as date) >"
        		+ " CAST(current_date as date) - CAST('7 days' as interval)"
        		+ " order by user_answer_date desc";
        try {
			pstmt = conn.prepareStatement(sql);
		} catch (SQLException e) {
			throw new SQLException();
		}
		return pstmt;
	}

	private final static String createSelectionSQL(String s, Integer c) {
		String ret = null;
		switch ((s)) {
		default:
			break;

		case "ア":
			ret = (new StringBuilder(" selection_rate_a = cast(")).append(Integer.toString(c.intValue()))
					.append(" as dec) * 100 / denominator.cnt").toString();
			break;

		case "イ":
			ret = (new StringBuilder(" selection_rate_i = cast(")).append(Integer.toString(c.intValue()))
					.append(" as dec) * 100 / denominator.cnt").toString();
			break;

		case "ウ":
			ret = (new StringBuilder(" selection_rate_u = cast(")).append(Integer.toString(c.intValue()))
					.append(" as dec) * 100 / denominator.cnt").toString();
			break;

		case "エ":
			ret = (new StringBuilder(" selection_rate_e = cast(")).append(Integer.toString(c.intValue()))
					.append(" as dec) * 100 / denominator.cnt").toString();
			break;

		case "？":
			ret = (new StringBuilder(" selection_rate_q = cast(")).append(Integer.toString(c.intValue()))
					.append(" as dec) * 100 / denominator.cnt").toString();
			break;
		}
		return ret;
	}

    private final static String getCurrentTimeStamp()
    {
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return format.format(date);
    }
}
