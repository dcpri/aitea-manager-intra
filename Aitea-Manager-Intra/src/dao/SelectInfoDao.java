// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3)
// Source File Name:   SelectInfoDao.java

package dao;

import java.util.ArrayList;

import beans.IndividuallyPageBeans;
import beans.RankingPageBeans;
import beans.WeeklyGraphBeans;
import beans.YearlyPageBeans;

public interface SelectInfoDao
{

    /**
     * @param category 試験区分
     * @param year 年度
     * @param turn 回次
     * @return 年次ページのBeanリスト
     * 年次ページの表示に必要な情報を返却する
     */
    public abstract ArrayList<YearlyPageBeans> selectYearly(String category, String year, String turn);

    /**
     * @param category 試験区分
     * @param id 問題ID
     * @return 個別ページのBeanリスト
     * 個別ページの表示に必要な情報を返却する
     */
    public abstract ArrayList<IndividuallyPageBeans> selectIndividually(String category, String id);

    /**
     * @param sort 昇順:asc 降順:desc
     * @param category 試験区分
     * @param year 年度
     * @param turn 回次
     * @return ランキングページのBeanリスト
     * ランキングページの表示に必要な情報を返却する
     */
    public abstract ArrayList<RankingPageBeans> selectRanking(String sort, String category, String year, String turn);

    /**
     * @return 週間グラフのBeanリスト
     * 週間グラフの表示に必要な情報を返却する
     */
    public abstract ArrayList<WeeklyGraphBeans> selectWeekly();
}
