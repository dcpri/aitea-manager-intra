// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3)
// Source File Name:   UpdateRateDao.java

package dao;

import java.util.ArrayList;
import java.util.HashMap;

import beans.CalcCorrectRateBeans;

public interface UpdateRateDao
{

    /**
     * @return 更新対象の問題リスト
     * 更新対象の問題を抽出する
     * 更新対象：プログラムが呼ばれた時間を基準に、
     * 24時間以内にサーバ受信情報テーブルに追加された問題
     */
    public abstract ArrayList<CalcCorrectRateBeans> selectUpdateQuestion();


    /**
     * @param exam_category 試験区分
     * @param question_id 問題番号
     * @return key:選択肢 value:総選択数のペア 総選択数=0の選択肢は追加されない
     * 更新対象の問題について、各選択肢の総選択数を求める
     */
    public abstract HashMap<String, Integer> selectSelectionRateAll(String exam_category,
    		String question_id);


    /**
     * @param exam_category 試験区分
     * @param question_id 問題番号
     * @param map key:選択肢 value:総選択数のペア
     * @return 更新成功or失敗
     * 正答率、選択肢選択率の更新を行う
     */
    public abstract boolean updateRates(String exam_category,
    		String question_id, HashMap<String, Integer> map);
}
