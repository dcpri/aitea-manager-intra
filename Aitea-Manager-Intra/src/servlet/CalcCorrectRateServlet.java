package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CalcCorrectRateBeans;
import dao.DaoFactory;
import dao.UpdateRateDao;

/**
 * @author joho
 * 正答率と選択肢選択率を計算するサーブレット
 */
@WebServlet("/CalcCorrectRateServlet")

public class CalcCorrectRateServlet extends HttpServlet
{

    private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        response.setContentType("text/html; charset=UTF-8");
        UpdateRateDao dao = DaoFactory.createUpdateRateDao();
        ArrayList<CalcCorrectRateBeans> beans = dao.selectUpdateQuestion();
        Iterator<CalcCorrectRateBeans> it = null;

        // アップデート件数のカウント
        Integer count;

        for(count = Integer.valueOf(0), it = beans.iterator(); it.hasNext(); count++)
        {
            CalcCorrectRateBeans bean = (CalcCorrectRateBeans)it.next();
            // 選択肢選択率の取得を行うSQLを発行
            HashMap<String, Integer> selectionMap = dao.selectSelectionRateAll(bean.getExam_category(), bean.getQuestion_id());
            // 正答率と選択肢選択率の更新を行うSQLを発行
            dao.updateRates(bean.getExam_category(), bean.getQuestion_id(), selectionMap);
        }

        response.getOutputStream().write(count.toString().getBytes("UTF-8"));
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        doGet(request, response);
    }
}
