package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.SelectInfoDao;
import net.sf.json.JSONArray;
import util.AiteaContract;
import util.AttributeValidator;
import util.QueryStringParser;


/**
 * @author joho
 * 問題個別ページのJSONを返却するサーブレット
 */
@WebServlet("/IndividuallyPageServlet")
public class IndividuallyPageServlet extends HttpServlet
{

    private static final long serialVersionUID = 1L;
    private String category = null;
    private String id  = null;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Cache-Control", "private");
        SelectInfoDao dao = DaoFactory.createSelectInfoDao();
        QueryStringParser p = new QueryStringParser(request.getQueryString());

        if(p.queryIsNull()){
            // クエリストリングが指定されなかった場合
        } else {
        	AttributeValidator v = new AttributeValidator();
            category =  p.get("category");
            id = p.get("id");

            if(v.validate(AiteaContract.VALIDATE_DATA.EXAM_CATEGORY, category) &&
               v.validate(AiteaContract.VALIDATE_DATA.Q_ID, id)){
            	JSONArray json = JSONArray.fromObject(dao.selectIndividually(category, id));
            	response.getOutputStream().write(json.toString().getBytes("UTF-8"));
            }
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        doGet(request, response);
    }
}
