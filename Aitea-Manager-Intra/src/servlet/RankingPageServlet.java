package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.SelectInfoDao;
import net.sf.json.JSONArray;
import util.AiteaContract;
import util.AttributeValidator;
import util.QueryStringParser;

/**
 * @author joho
 * ランキング表示ページのJSONを返却するサーブレット
 */
@WebServlet("/RankingPageServlet")
public class RankingPageServlet extends HttpServlet
{

    private static final long serialVersionUID = 1L;
    private String sort;
    private String category;
    private String year;
    private String turn;

    public RankingPageServlet()
    {
        sort = null;
        category = null;
        year = null;
        turn = null;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Cache-Control", "private");
        SelectInfoDao dao = DaoFactory.createSelectInfoDao();

        // クエリストリングの取得と、データ形式の変換
        QueryStringParser p =  new QueryStringParser(request.getQueryString());
        if(p.queryIsNull()){

        } else {
            sort = translateSort(p.get("sort"), p.get("order"));
            category = p.get("category");
            year = translateYear(p.get("year"));
            turn = translateTurn(p.get("turn"));
        }

        AttributeValidator v = new AttributeValidator();
        // データチェック
        if(v.validate(AiteaContract.VALIDATE_DATA.EXAM_CATEGORY, category) &&
           v.validate(AiteaContract.VALIDATE_DATA.YEAR, year) &&
           v.validate(AiteaContract.VALIDATE_DATA.TURN, turn) ){
        	JSONArray json = JSONArray.fromObject(dao.selectRanking(sort, category, year, turn));
        	response.getOutputStream().write(json.toString().getBytes("UTF-8"));
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        doGet(request, response);
    }

    private String translateSort(String sort, String order)
    {
        String result = null;
        if(sort == null)
            return "cnt desc nulls last";
        if(order == null)
            return "cnt desc nulls last";
        if(sort.equals("CNT"))
        {
            if(order.equals("ASC"))
                result = "cnt asc";
            else
            if(order.equals("DESC"))
                result = "cnt desc";
        } else
        if(sort.equals("CORRECT"))
            if(order.equals("ASC"))
                result = "question_correct_rate asc";
            else
            if(order.equals("DESC"))
                result = "question_correct_rate desc";
        return (new StringBuilder(String.valueOf(result))).append(" nulls last").toString();
    }

    private String translateYear(String year)
    {
    	if(year.equals("EMPTY")) return "EMPTY";
        String era = null;
        switch(year.charAt(0))
        {
        case 'H': // 'H'
            era = "平成";
            break;
        }
        return (new StringBuilder(String.valueOf(era))).append(year.substring(1)).append("年").toString();
    }

    private String translateTurn(String turn)
    {
    	if(turn.equals("EMPTY")) return "EMPTY";
    	return turn.equals("S") ? "春期" : "秋期";
    }
}
