package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.SelectInfoDao;
import net.sf.json.JSONArray;

/**
 * @author joho
 * 週次ページのJSONを返却するサーブレット
 */
@WebServlet("/WeeklyGraphServlet")
public class WeeklyGraphServlet extends HttpServlet
{

    private static final long serialVersionUID = 1L;

    public WeeklyGraphServlet()
    {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Cache-Control", "private");
        SelectInfoDao dao = DaoFactory.createSelectInfoDao();
        JSONArray json = JSONArray.fromObject(dao.selectWeekly());
        response.getOutputStream().write(json.toString().getBytes("UTF-8"));
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        doGet(request, response);
    }

}
