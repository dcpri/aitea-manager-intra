package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.SelectInfoDao;
import net.sf.json.JSONArray;
import util.AiteaContract;
import util.AttributeValidator;
import util.QueryStringParser;

/**
 * @author joho
 * 年次ページのJSONを返却するサーブレット
 */
@WebServlet("/YearlyPageServlet")
public class YearlyPageServlet extends HttpServlet
{

    private static final long serialVersionUID = 1L;
    private String category;
    private String year;
    private String turn;

    public YearlyPageServlet()
    {
        category = null;
        year = null;
        turn = null;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Cache-Control", "private");
        SelectInfoDao dao = DaoFactory.createSelectInfoDao();
        QueryStringParser p = new QueryStringParser(request.getQueryString());
        if(p.queryIsNull())
        {
            // クエリストリングが指定されなかった場合
        } else
        {
            category = p.get("category");
            year = translateYear(p.get("year"));
            turn = translateTurn(p.get("turn"));
        }

        AttributeValidator v = new AttributeValidator();
        if(v.validate(AiteaContract.VALIDATE_DATA.EXAM_CATEGORY, category) &&
           v.validate(AiteaContract.VALIDATE_DATA.YEAR, year) &&
           v.validate(AiteaContract.VALIDATE_DATA.TURN, turn) ){
        	JSONArray json = JSONArray.fromObject(dao.selectYearly(category, year, turn));
        	response.getOutputStream().write(json.toString().getBytes("UTF-8"));
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        doGet(request, response);
    }

    private String translateYear(String year)
    {
        String era = null;
        switch(year.charAt(0))
        {
        case 'H': // 'H'
            era = "平成";
            break;
        }
        return (new StringBuilder(String.valueOf(era))).append(year.substring(1)).append("年").toString();
    }

    private String translateTurn(String turn)
    {
        return turn.equals("S") ? "春期" : "秋期";
    }
}
