package util;

public final class AiteaContract {

    public static final String MY_EXAM_CATEGORY = "AP";

    public static abstract class VALIDATE_DATA {

    	public static final String EXAM_CATEGORY = "exam_category";
    	public static final String Q_ID = "question_id";
    	public static final String USER_ANSWER = "user_answer";
    	public static final String CORRECT_MISTAKE = "correct_mistake";
        public static final String YEAR = "year";
        public static final String TURN = "turn";
    }

    public static abstract class QUESTION_INFO  {

        public static final String TABLE_NAME = "tbl_question_info";
        public static final String EXAM_CATEGORY = "exam_category";
        public static final String Q_ID = "question_id";
        public static final String YEAR = "year";
        public static final String TURN = "turn";
        public static final String Q_NO = "question_no";
        public static final String FIELD_CODE = "field_code";
        public static final String L_CLASS_CODE = "large_class_code";
        public static final String M_CLASS_CODE = "middle_class_code";
        public static final String S_CLASS_CODE = "small_class_code";
        public static final String Q_NAME = "question_name";
        public static final String Q_SENTENCE = "question_sentence";
        public static final String Q_IMAGE = "question_image";
        public static final String SELECTION_SENTENCE = "selection_sentence";
        public static final String SELECTION_IMAGE = "selection_image";
        public static final String CORRECT_ANSWER = "correct_answer";
        public static final String Q_CORRECT_RATE = "question_correct_rate";
        public static final String SELECTION_RATE_A = "selection_rate_a";
        public static final String SELECTION_RATE_I = "selection_rate_i";
        public static final String SELECTION_RATE_U = "selection_rate_u";
        public static final String SELECTION_RATE_E = "selection_rate_e";
        public static final String SELECTION_RATE_Q = "selection_rate_q";
    }

    public static abstract class GENRE_INFO  {

    	public static final String EXAM_CATEGORY = "exam_category";
        public static final String TABLE_NAME = "tbl_genre_info";
        public static final String FIELD_CODE = "field_code";
        public static final String FIELD_NAME = "field_name";
        public static final String L_CLASS_CODE = "large_class_code";
        public static final String L_CLASS_NAME = "large_class_name";
        public static final String M_CLASS_CODE = "middle_class_code";
        public static final String M_CLASS_NAME = "middle_class_name";
        public static final String S_CLASS_CODE = "small_class_code";
        public static final String S_CLASS_NAME = "small_class_name";
    }

    public static abstract class SERVER_RECEIVE_INFO  {

        public static final String TABLE_NAME = "tbl_server_receive_info";
        public static final String RECEIVE_ID = "receive_id";
        public static final String EXAM_CATEGORY = "exam_category";
        public static final String QUESTION_ID = "question_id";
        public static final String USER_ANSWER = "user_answer";
        public static final String CORRECT_MISTAKE = "correct_mistake";
        public static final String USER_ANSWER_DATE = "user_answer_date";
    }
}
