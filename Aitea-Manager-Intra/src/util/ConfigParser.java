/**
 *
 */
package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author joho
 * コンフィグファイルの読み込みと値の取り出しを行うクラス
 */
public class ConfigParser {

	private Properties conf = new Properties();

	/**
	 * @param filename コンフィグファイルの名前
	 * コンフィグファイルの読み込みを行う
	 */
	public ConfigParser(String filename) throws FileNotFoundException, IOException{
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(new File(filename));
		} catch (FileNotFoundException e) {
			throw new FileNotFoundException(e.getMessage());
		}

		try {
			conf.load(inputStream);
		} catch (IOException e) {
			throw new IOException(e.getMessage());
		}
	}

	/**
	 * @param key コンフィグファイルのキー
	 * @return コンフィグファイルのバリュー
	 */
	public String get(String key){
		String ret = conf.getProperty(key);

		if(ret == null){
			return null;
		} else {
			return ret;
		}
	}

}
