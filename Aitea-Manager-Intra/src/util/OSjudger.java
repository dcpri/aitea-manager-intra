/**
 *
 */
package util;

/**
 * @author joho
 * OSの種類を判定するクラス
 */
public class OSjudger {

	private static final String OS_NAME = System.getProperty("os.name").toLowerCase();

	/**
	 * @return true:OSがLinux系である false:OSがLinux系でない
	 */
	public static boolean isLinux(){
		return OS_NAME.startsWith("linux");
	}

}
